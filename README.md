# Learn Docker #

Link Dokumentasi: (https://drive.google.com/file/d/1HzREciZ6dagLgvbIzj558W2X3gECXSHE/view?usp=sharing)

> Berikut beberapa perintah dasar pada Docker dalam pembuatan suatu container.

-	docker info: mendapatkan informasi detail dari docker yang dingunakan.

-	docker images: menampilkan semua image yang terdapat di local komputer.
 
-	docker pull <nama_images>: mendownload image dari registry docker bernama docker hub

-	docker container ls: menampilkan daftar container yang sedang berjalan
 
-	docker container ls -all: menampilkan semua daftar container baik yang sedang berjalan maupun tidak berjalan.
 
-	docker container create –name <nama_container> <nama_images>: membuat container baru menggunakan image yang ditujukan beserta nama container yang diinginkan.

-	docker container start mongoserver1: membuat container baru menggunakan image yang ditujukan.
 
-	docker container stop mongoserver1: perintah untukmenghentikan container bernama mongoserver1
 
-	docker container rm mongoserver1: perintah menghapus container bernama mongoserver1
 
-	docker image rm mongo: perintah menghapus images bernama mongo
 


> Membuat dockerfile yang berisikan aplikasi didalamnya
-	docker build –tag testweb:1.0 . : perintah untuk membuat image baru dengan nama testweb dan tag 1.0 pada dockerfile
 
-	docker images: menampilkan semua image yang terdapat di local komputer. 

-	docker run --name templateweb  -p 41060:22 -p 8000:80 -d testweb:1.0 : perintah untuk menjalankan image ‘testweb:1.0’ untuk membuat container bernama templateweb dengan memforward port 41060 ke port 22 dan port 8080 ke port 80.

-	docker container ls: menampilkan container yang sedang berjalan.  

-	localhost:8000/testhtml : mencoba mengakses localhost pada port 8000 pada browser.
 
-	docker container stop templateweb: menghentikan container ‘templateweb’ yang sedang berjalan.
 
-	docker container rm templateweb: menghapus container ‘templateweb’.
 
-	docker login : perintah untuk login ke dalam docker hub agar dapat melakukan push/upload images ke repository
 
-	docker login : perintah untuk push image ke dalam repository akun yang dituju.

-	docker image rm testweb:1.0 : perintah untuk menghapus image yang berada di local.
 
